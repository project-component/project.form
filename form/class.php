<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Main\Application,
    Bitrix\Main\Mail\Event,
    Project\Tools;

class projectForm extends projectComponent {

    protected function projectFormGetName($arFields, $propFields) {
        $arData = array(
            'Заявка:',
            date('d.m.Y H:i:s')
        );
        return implode(' ', $arData);
    }

    protected function projectFormInitElement() {
        $this->arResult['FIELD'] = array();
        if (!empty($this->arParams['FIELD_CODE'])) {
            foreach ($this->arParams['FIELD_CODE'] as $value) {
                if ($value) {
                    $this->arResult['FIELD'][$value] = array(
                        'CODE' => $value,
                        'REQUIRED' => in_array($value, $this->arParams['REQUIRED_FIELD_CODE'])
                    );
                }
            }
        }
        if (!empty($this->arParams['PROPERTY_CODE'])) {
            foreach ($this->arParams['PROPERTY_CODE'] as $value) {
                if ($value) {
                    $this->arResult['FIELD']['PROPERTY_' . $value] = array(
                        'CODE' => $value,
                        'PROPERTY' => true,
                        'REQUIRED' => in_array($value, $this->arParams['REQUIRED_PROPERTY_CODE'])
                    );
                }
            }
        }
    }

    protected function projectFormInit() {
        $arParams = $this->arParams;
        $arParams['FILTER'] = $arParams['WRAPPER']['FILTER'] ?: '';
        $arParams['PAGEN'] = $arParams['WRAPPER']['PAGEN'] ?: '';
        unset($arParams['WRAPPER'], $arParams['~WRAPPER']);
        $this->arResult['HASH'] = sha1(serialize($arParams));
        $this->arResult['IS_SEND'] = false;
        $this->arResult['ERROR'] = array();
        $this->arResult['ERROR_FORM'] = array();
        return $this->projectFormInitElement();
    }

    protected function projectIsFormSubmit() {
        $request = Application::getInstance()->getContext()->getRequest();
        return $request->getPost('HASH') === $this->arResult['HASH'];
    }

    protected function projectFormSend($arData) {
        pre($arData);
    }

    public function executeComponent() {
        $this->projectFilterParam('iblock', 'section');
        if (self::projectLoader('iblock')) {
            $this->projectFormInit();
            if ($this->projectIsFormSubmit()) {
                list(, $iblock) = $this->projectGetFilter('iblock', 'section');
                $request = Application::getInstance()->getContext()->getRequest();
                if (defined('Project\Tools\IS_START')) {
                    $this->arResult['FIELD_NAME'] = Tools\Utility\Cache::get(array(__CLASS__, __FUNCTION__, $iblock), function() use($iblock) {
                                $arResult = array();
                                $rsProp = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock['IBLOCK_ID']));
                                while ($arr = $rsProp->Fetch()) {
                                    $arResult['PROPERTY_' . $arr["CODE"]] = $arr["NAME"];
                                }
                                return $arResult;
                            });
                }
                $arFields = $propFields = $arSend = array();
                foreach ($this->arResult['FIELD'] as $key => $arItem) {
                    if ($value = $request->getPost($key)) {
                        $arSend[] = $this->arResult['FIELD_NAME'][$key] . ': ' . $value;
                        if (isset($arItem['PROPERTY'])) {
                            $propFields[$arItem['CODE']] = $value;
                        } else {
                            $arFields[$arItem['CODE']] = $value;
                        }
                    }
                    $this->arResult['VALUE'][$key] = htmlspecialchars($value, ENT_QUOTES);
                    if ($arItem['REQUIRED'] and $value === '') {
                        $this->arResult['ERROR'][$key] = 'Поле «' . $this->arResult['FIELD_NAME'][$key] . '» не заполнено';
                        $this->arResult['ERROR'][$key] = 'Поле не заполнено';
                    }
                }
//                pre($this->arResult['ERROR']);
                if (empty($this->arResult['ERROR'])) {
                    $arFields['DATE_ACTIVE_FROM'] = ConvertTimeStamp(time(), 'FULL');
                    $arFields['IBLOCK_ID'] = $iblock['IBLOCK_ID'];
                    $arFields['IBLOCK_SECTION_ID'] = $this->arParams['SECTION_ID'];
                    $arFields['NAME'] = $this->projectFormGetName($arFields, $propFields);
                    $arFields['ACTIVE'] = 'Y';
                    $el = new CIBlockElement;
                    if (!$arFields['ID'] = $el->Add($arFields)) {
                        $this->arResult['ERROR_FORM'][] = $el->LAST_ERROR;
                    } else {
                        foreach ($propFields as $key => $value) {
                            CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], $value, $key);
                        }
                    }
//                    Event::send(array(
//                        "EVENT_NAME" => $this->arParams['EVENT_NAME'],
//                        "LID" => "s1",
//                        "C_FIELDS" => array(
//                            'FORM_DATA' => implode('<br>', $arSend)
//                        ),
//                    ));
                    CEvent::SendImmediate($this->arParams['EVENT_NAME'], "s1", array(
                            'FORM_DATA' => implode('<br>', $arSend)
                        ));
                    $arResult['VALUE'] = array();
                    $this->arResult['IS_SEND'] = true;
                }
//                pre($arFields, $propFields, $arSend);
            }
            $this->projectTemplate();
        }
    }

}
