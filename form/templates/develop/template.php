<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <div class="<?= $arParams['WRAPPER']["CONTANER"] ?>">
    <? } ?>
    <? if ($arResult['IS_SEND']) { ?>
        <div class="desctop__form popup__top">
            <div class="popup__title">Заявка принята!</div>
            <div class="popup__title-text">В течение 15 минут с Вами свяжется наш менеджер, чтобы обсудить детали дальнейшего взаимодействия.</div>
        </div>
    <? } else { ?>
        <form class="desctop__form <?= $arParams['WRAPPER']['CONTANER_FORM'] ?>">
            <? if (!empty($arResult['ERROR_FORM'])) { ?>
                <div class="cooperate__wrap-input required">
                    <? ShowMessage(implode('<br>', $arResult['ERROR_FORM'])) ?>
                </div>
            <? } ?>
            <input type="hidden" name="HASH" value="<?= $arResult['HASH'] ?>">
            <div class="cooperate__wrap-input required">
                <span class="cooperate__input_name">
                    Имя
                    <? if (isset($arResult['ERROR']['PROPERTY_NAME'])) { ?>
                        <? ShowMessage($arResult['ERROR']['PROPERTY_NAME']) ?>
                    <? } ?>
                </span>
                <input type="text" name="PROPERTY_NAME" placeholder="Представьтесь, пожалуйста" required="" value="<?= $arResult['VALUE']['PROPERTY_NAME'] ?: '' ?>">
            </div>

            <div class="cooperate__wrap-input required">
                <span class="cooperate__input_name">
                    Телефон
                    <? if (isset($arResult['ERROR']['PROPERTY_PHONE'])) { ?>
                        <? ShowMessage($arResult['ERROR']['PROPERTY_PHONE']) ?>
                    <? } ?>
                </span>
                <input class="phone-mask-js" type="text" name="PROPERTY_PHONE" required="" placeholder="" value="<?= $arResult['VALUE']['PROPERTY_PHONE'] ?: '' ?>">
            </div>

            <div class="cooperate__wrap-input">
                <span class="cooperate__input_name">
                    Почта
                    <? if (isset($arResult['ERROR']['PROPERTY_EMAIL'])) { ?>
                        <? ShowMessage($arResult['ERROR']['PROPERTY_EMAIL']) ?>
                    <? } ?>
                </span>
                <input type="text" name="PROPERTY_EMAIL" placeholder="Укажите эелектронную почту" value="<?= $arResult['VALUE']['PROPERTY_EMAIL'] ?: '' ?>">
            </div>

            <div class="cooperate__wrap-input last">
                <span class="cooperate__input_name textarea">
                    Сообщение
                    <? if (isset($arResult['ERROR']['PROPERTY_MESSAGE'])) { ?>
                        <? ShowMessage($arResult['ERROR']['PROPERTY_MESSAGE']) ?>
                    <? } ?>
                </span>
                <textarea name="PROPERTY_MESSAGE" placeholder="Здесь вы можете написать свой вопрос
                          или комментарий"><?= $arResult['VALUE']['PROPERTY_MESSAGE'] ?: '' ?></textarea>
            </div>

            <div class="cooperate__submit-block">
                <div class="cooperate__submit-block--text">поля, обязательные<br>
                    для заполнения</div>
                <input type="submit" name="" value="Начать сотрудничество" class="orange_bth">
            </div>
        </form>
    <? } ?>
    <? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    </div>
<? } ?>

