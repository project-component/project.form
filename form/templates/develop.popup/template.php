<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <section class="popup">
        <div class="<?= $arParams['WRAPPER']["CONTANER"] ?>">
        <? } ?>
        <? if ($arResult['IS_SEND']) { ?>
            <script>
                $('.popup').removeClass('active');
                $('.successful').show();
            </script>
        <? }?>
            <div class="popup__top">
                <div class="popup__title">Начнем
                    сотрудинчество?</div>
                <div class="popup__close popup__close-js"></div>
            </div>
            <form class="popup__form <?= $arParams['WRAPPER']['CONTANER_FORM'] ?>">
                <? if (!empty($arResult['ERROR_FORM'])) { ?>
                    <div class="popup__wrap required">
                        <? ShowMessage(implode('<br>', $arResult['ERROR_FORM'])) ?>
                    </div>
                <? } ?>
                <input type="hidden" name="HASH" value="<?= $arResult['HASH'] ?>">
                <div class="popup__wrap required">
                    <div class="popup__inp-title">
                        Имя
                        <? if (isset($arResult['ERROR']['PROPERTY_NAME'])) { ?>
                            <? ShowMessage($arResult['ERROR']['PROPERTY_NAME']) ?>
                        <? } ?>
                    </div>
                    <input type="text" name="PROPERTY_NAME" placeholder="Представьтесь, пожалуйста" required="" value="<?= $arResult['VALUE']['PROPERTY_NAME'] ?: '' ?>">
                </div>
                <div class="popup__wrap required">
                    <div class="popup__inp-title">
                        Телефон
                        <? if (isset($arResult['ERROR']['PROPERTY_PHONE'])) { ?>
                            <? ShowMessage($arResult['ERROR']['PROPERTY_PHONE']) ?>
                        <? } ?>
                    </div>
                    <input type="text" name="PROPERTY_PHONE" placeholder="+7 (___) ___ __ __" class="phone-mask-js" required="" value="<?= $arResult['VALUE']['PROPERTY_PHONE'] ?: '' ?>">
                </div>
                <div class="popup__wrap">
                    <div class="popup__inp-title">
                        Почта
                        <? if (isset($arResult['ERROR']['PROPERTY_EMAIL'])) { ?>
                            <? ShowMessage($arResult['ERROR']['PROPERTY_EMAIL']) ?>
                        <? } ?>
                    </div>
                    <input type="text" name="PROPERTY_EMAIL" placeholder="Укажите эелектронную почту" value="<?= $arResult['VALUE']['PROPERTY_EMAIL'] ?: '' ?>">
                </div>
                <div class="popup__wrap last_pp">
                    <div class="popup__inp-title">
                        Сообщение
                        <? if (isset($arResult['ERROR']['PROPERTY_MESSAGE'])) { ?>
                            <? ShowMessage($arResult['ERROR']['PROPERTY_MESSAGE']) ?>
                        <? } ?>
                    </div>
                    <textarea name="PROPERTY_MESSAGE" placeholder="Здесь вы можете написать свой вопрос
или комментарий"><?= $arResult['VALUE']['PROPERTY_MESSAGE'] ?: '' ?></textarea>
                </div>
                <div class="popup__bottom">
                    <div class="warning__messages">поля, обязательные
                        для заполнения</div>
                    <input type="submit" name="" value="Начать сотрудничество" class="orange_bth popup__submit">
                </div>
            </form>
        <? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
        </div>
    </section>
<? } ?>

